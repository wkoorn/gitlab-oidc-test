import os

import pytest
from fastapi.testclient import TestClient

from app import app


def test_app():
    if os.getenv("CI") != "true":
        pytest.skip("Not running in CI/CD pipeline")

    token = os.getenv("EXAMPLE_ID_TOKEN")
    assert token

    test_client = TestClient(app)
    response = test_client.get("/me", headers={"Authorization": token})

    assert response.status_code == 200
    assert response.json() == os.getenv("GITLAB_USER_LOGIN")
