import typing
from typing import Iterable

import fastapi.security
import httpx
import pydantic
from joserfc import jwt, jwk
from joserfc.errors import JoseError

OPENID_CONFIG_URL = "https://gitlab.cern.ch/.well-known/openid-configuration"

gitlab_oidc = fastapi.security.OpenIdConnect(
    openIdConnectUrl=OPENID_CONFIG_URL,
    scheme_name="GitLab OpenID Connect Auth",
    description=(
        "See https://docs.gitlab.com/ee/integration/openid_connect_provider.html"
        "and https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html"
    )
)


class GitLabIdTokenClaims(pydantic.BaseModel):
    aud: str | Iterable[str]
    namespace_id: int | Iterable[int] | None = None
    namespace_path: str | Iterable[str] | None = None
    project_id: int | Iterable[int] | None = None
    project_path: str | Iterable[str] | None = None
    user_id: int | Iterable[int] | None = None
    user_login: str | Iterable[str] | None = None
    user_email: str | Iterable[str] | None = None
    pipeline_id: int | Iterable[int] | None = None
    pipeline_source: str | Iterable[str] | None = None
    job_id: int | Iterable[int] | None = None
    ref: str | Iterable[str] | None = None
    ref_type: str | Iterable[str] | None = None
    ref_path: str | Iterable[str] | None = None
    ref_protected: str | Iterable[str] | None = None
    runner_id: int | Iterable[int] | None = None
    sha: str | Iterable[str] | None = None
    jti: str | Iterable[str] | None = None
    iat: int | Iterable[int] | None = None
    nbf: int | Iterable[int] | None = None
    exp: int | Iterable[int] | None = None
    sub: str | Iterable[str] | None = None
    runner_environment: str | Iterable[str] | None = "self-hosted"
    iss: str | Iterable[str] | None = "https://gitlab.cern.ch"


class GitLabOIDC:
    def __init__(self, validate_claims: GitLabIdTokenClaims):
        self.claim_registry = jwt.JWTClaimsRegistry(**{
            key: jwt.ClaimsOption(
                essential=True,
                values=[value] if isinstance(value, (str, int)) else list(value),
            ) for key, value in validate_claims if value is not None
        })
        self.http_client = httpx.AsyncClient()

    async def __call__(
            self,
            request: fastapi.Request,
            id_token: typing.Annotated[str, fastapi.Depends(gitlab_oidc)],
    ) -> GitLabIdTokenClaims:
        openid_config_response = await self.http_client.get(OPENID_CONFIG_URL)
        if openid_config_response.status_code != 200:
            raise RuntimeError("Failed to get GitLab OIDC config")
        openid_config = openid_config_response.json()

        key_set_response = await self.http_client.get(openid_config["jwks_uri"])
        if key_set_response.status_code != 200:
            raise RuntimeError("Failed to get GitLab OIDC keys")
        key_set = jwk.KeySet.import_key_set(key_set_response.json())

        claims = jwt.decode(id_token, key=key_set).claims

        try:
            self.claim_registry.validate(claims)
        except JoseError as e:
            msg = "Invalid GitLab ID token: " + e.description
            raise fastapi.HTTPException(401, msg)

        return GitLabIdTokenClaims.model_validate(claims)
