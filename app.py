import typing

import uvicorn
from fastapi import FastAPI, Depends

from lib import GitLabIdTokenClaims, GitLabOIDC

app = FastAPI()

GitLabAuthentication = typing.Annotated[
    GitLabIdTokenClaims,
    Depends(GitLabOIDC(validate_claims=GitLabIdTokenClaims(
        aud="gitlab-oidc-example",
    ))),
]


@app.get("/me")
def me(claims: GitLabAuthentication) -> str:
    return claims.user_login


if __name__ == '__main__':
    uvicorn.run(app)
